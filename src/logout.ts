import { Builder, By } from "selenium-webdriver";
import { Options } from "selenium-webdriver/firefox";

const firefoxOptions = new Options();
firefoxOptions.headless();

(async function main() {
  const driver = await new Builder()
    .forBrowser("firefox")
    .setFirefoxOptions(firefoxOptions)
    .build();
  try {
    driver.get("http://tree.buct.edu.cn");
    await driver.sleep(1000);
    const logoutBtn = await driver.findElement(By.id("logout"));
    await logoutBtn.click();
  } catch (err) {
    console.log(err);
  }
  await driver.sleep(1000);
  driver.quit();
})();
