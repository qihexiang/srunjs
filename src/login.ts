import { Builder, By } from "selenium-webdriver";
import {Options} from 'selenium-webdriver/firefox'
import { username, password } from "../config.json";

const firefoxOptions = new Options();
firefoxOptions.headless();

(async function main() {
  const driver = await new Builder()
    .forBrowser("firefox")
    .setFirefoxOptions(firefoxOptions)
    .build();
  try {
    driver.get("http://tree.buct.edu.cn");
    await driver.sleep(1000);
    await Promise.all([
      driver.findElement(By.id("username")).sendKeys(username),
      driver.findElement(By.id("password")).sendKeys(password),
    ])
      .then((res) => {
        return driver.findElement(By.id("login")).click();
      })
      .catch((err) => {
        throw err;
      });
  } catch (err) {
    console.log(err);
  }
  await driver.sleep(1000);
  driver.quit();
})();
