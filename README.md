# 北京化工大学校园网登录工具

使用此工具可以完成北京化工大学校园网的登录和注销动作。

## 准备工作

### 本地预备（需要Node.js）

```
git clone https://gitee.com/qihexiang/srunjs.git
cd srunjs
npm install && npm run package
scp ../srunjs_release.tar.gz username@server_host:./
```

### 服务器部署（安装Node.js、Geckodriver、Firefox）

```
mkdir srunjs
cd srunjs
tar zxfv ../srunjs_release.tar.gz
vi config.json
```

之后写入配置。

## 配置文件

在此目录下创建配置文件`config.json`，包含`username`和`password`两项，均为string类型（用户名是数字，用双引号包裹为字符串），如下：

```json
{
    "username": "2017060011",
    "password": "password is here"
}
```

## 使用

- 登录：`npm run login`
- 注销：`npm run logout`

## 与srunauth的区别

是srunauth的简化版本，不支持命令行形式的配置、状态检查功能和自动操作功能。